FROM ubuntu:18.04

MAINTAINER Farhan "farhan@daruma.co.id"

RUN apt-get update -y && apt-get install -y -q python3 python3-pip

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install -r requirements.txt

RUN pip3 install elasticsearch

ARG GOOGLE_APPLICATION_CREDENTIALS="/app/service-account-file.json"

ENV GOOGLE_APPLICATION_CREDENTIALS="/app/service-account-file.json"

COPY . /app

ENTRYPOINT [ "python3" ]

CMD [ "postgresql.py" ]