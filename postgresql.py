import psycopg2 as pg
from db_config import config
import ast
import pprint
import json
from datetime import datetime
import time
import os
import requests
import sys
from push_to_elasticsearch import insert_docs, update_elasticsearch_doc, update_brand_elasticsearch_doc
from urllib.parse import urlparse
import _constants
import pprint
from rules import *

params = config()

def connect():
    conn = None
    try:
        print("Connecting to the postgresql database..")
        conn = pg.connect(**params)

        cur = conn.cursor()

        print("Postgres Database Version:")
        cur.execute("SELECT version()")

        db_version = cur.fetchone()
        print(db_version)

        cur.close()
        print("cursor closed..")
    except (Exception, pg.DatabaseError) as e:
        print("ERROR from postgres.py, connect(): ", e)
    finally:
        if conn:
            conn.close()
            print("connection closed..")

def create_tables():
    """ create tables """

    # commands = """
    #        CREATE TABLE users (
    #             user_id SERIAL PRIMARY KEY,
    #             username VARCHAR(50) UNIQUE NOT NULL,
    #             password VARCHAR(50)
    #         );
    #         """

    # commands = """
    #            CREATE TABLE task (
    #                 task_id SERIAL PRIMARY KEY,
    #                 task_name VARCHAR(255) UNIQUE NOT NULL,
    #                 index_url VARCHAR(255),
    #                 product_url VARCHAR(255),
    #                 brand_filter VARCHAR(200),
    #                 detail_filter VARCHAR(255),
    #                 task_type VARCHAR(50),
    #                 status VARCHAR(50) DEFAULT 'incomplete',
    #                 requested_by VARCHAR(100)
    #             )
    #             """

    # commands = (
    #     """
    #        CREATE TABLE job (
    #             job_id SERIAL PRIMARY KEY,
    #             url VARCHAR(200),
    #             daruma_code VARCHAR(50),
    #             created_at timestamp without time zone NOT NULL DEFAULT now(),
    #             created_by VARCHAR(150),
    #             start_time VARCHAR(50),
    #             end_time VARCHAR(50),
    #             error_count INTEGER
    #         )
    #     """,
    #     """
    #        CREATE TABLE request (
    #             job_id INTEGER,
    #             url VARCHAR,
    #             http_status_code VARCHAR(50),
    #             content_type VARCHAR(150),
    #             content_length VARCHAR(50),
    #             created_at timestamp without time zone NOT NULL DEFAULT now(),
    #             exception_content VARCHAR(250),
    #             FOREIGN KEY (job_id) REFERENCES job (job_id)
    #             ON UPDATE CASCADE ON DELETE CASCADE
    #         )
    #     """,
    #     """
    #        CREATE TABLE item (
    #             item_id SERIAL PRIMARY KEY,
    #             price INTEGER,
    #             url VARCHAR(250),
    #             data VARCHAR,
    #             created_at timestamp without time zone NOT NULL DEFAULT now()
    #         )
    #     """
    # )

    # commands = ("""
    #                        CREATE TABLE users (
    #                          user_id SERIAL PRIMARY KEY,
    #                          username VARCHAR(50) UNIQUE,
    #                          password VARCHAR(50)
    #                         )
    #             """,)

    # commands = ("""
    #                    CREATE TABLE job (
    #                         job_id SERIAL PRIMARY KEY,
    #                         url VARCHAR,
    #                         daruma_code VARCHAR(50),
    #                         created_at timestamp without time zone NOT NULL DEFAULT now(),
    #                         created_by VARCHAR(150),
    #                         start_time timestamp without time zone,
    #                         end_time timestamp without time zone,
    #                         error_count INTEGER,
    #                         status VARCHAR DEFAULT 'pending'
    #                     )
    #                 """,
    #                 """
    #                        CREATE TABLE request (
    #                          job_id INTEGER,
    #                          url VARCHAR,
    #                          http_status_code VARCHAR(50),
    #                          content_type VARCHAR(150),
    #                          content_length VARCHAR(50),
    #                          created_at timestamp without time zone NOT NULL DEFAULT now(),
    #                          exception_content VARCHAR,
    #                          FOREIGN KEY (job_id) REFERENCES job (job_id)
    #                          ON UPDATE CASCADE ON DELETE CASCADE
    #                         )
    #                 """,
    #             """
    #                                CREATE TABLE item (
    #                                     item_id SERIAL PRIMARY KEY,
    #                                     job_id INTEGER,
    #                                     url VARCHAR,
    #                                     daruma_code VARCHAR(100),
    #                                     data VARCHAR NOT NULL,
    #                                     created_at timestamp without time zone NOT NULL DEFAULT now()
    #                                 )
    #             """,
    #             )

    # id | name | brand | price_before_discount | price | sku | supplier_code | description | item_url
    # | store | last_update | daruma_code | rating
    # | store_url | image_url | link_id

    commands_product = ("""
                               CREATE TABLE product (
                                 link_id VARCHAR PRIMARY KEY,
                                 name VARCHAR,
                                 brand VARCHAR,
                                 sku VARCHAR,
                                 supplier_code VARCHAR,
                                 description VARCHAR,
                                 last_update timestamp without time zone NOT NULL DEFAULT now()
                                )

                    """,
                )

    # commands = ("""
    #                        CREATE TABLE price (
    #                          id SERIAL,
    #                          name VARCHAR,
    #                          brand VARCHAR,
    #                          price_before_discount VARCHAR,
    #                          price VARCHAR,
    #                          sku VARCHAR,
    #                          supplier_code VARCHAR,
    #                          description VARCHAR,
    #                          item_url VARCHAR PRIMARY KEY,
    #                          store VARCHAR,
    #                          last_update timestamp without time zone NOT NULL DEFAULT now(),
    #                          daruma_code VARCHAR,
    #                          rating INTEGER DEFAULT 0
    #                         )
    #
    #             """,
    #             )
    #             """
    #                         CREATE TABLE store (
    #                              item_url VARCHAR UNIQUE NOT NULL,
    #                              name VARCHAR,
    #                              store_url VARCHAR,
    #                              type VARCHAR,
    #                              FOREIGN KEY (item_url)
    #                              REFERENCES price (item_url)
    #                              ON UPDATE CASCADE ON DELETE CASCADE
    #                             );
    #             """)

    # commands = ("""
    #                            CREATE TABLE store (
    #                              item_url VARCHAR UNIQUE NOT NULL,
    #                              name VARCHAR,
    #                              store_url VARCHAR,
    #                              type VARCHAR,
    #                              FOREIGN KEY (item_url)
    #                              REFERENCES price (item_url)
    #                              ON UPDATE CASCADE ON DELETE CASCADE
    #                             );
    #                 """,)
    #
    # commands = ("""
    #
    # CREATE TABLE item (
    #                                     item_id SERIAL PRIMARY KEY,
    #                                     job_id INTEGER,
    #                                     url VARCHAR,
    #                                     daruma_code VARCHAR(100),
    #                                     data VARCHAR NOT NULL,
    #                                     created_at timestamp without time zone NOT NULL DEFAULT now(),
    #                                     status VARCHAR DEFAULT 'not inserted',
    #                                     spider VARCHAR
    #                                 );
    #
    # """,
    # """
    #                                        CREATE TABLE price (
    #                                          id SERIAL,
    #                                          name VARCHAR,
    #                                          brand VARCHAR,
    #                                          price_before_discount VARCHAR,
    #                                          price VARCHAR,
    #                                          sku VARCHAR,
    #                                          supplier_code VARCHAR,
    #                                          description VARCHAR,
    #                                          item_url VARCHAR PRIMARY KEY,
    #                                          store VARCHAR,
    #                                          last_update timestamp without time zone NOT NULL DEFAULT now(),
    #                                          daruma_code VARCHAR,
    #                                          rating INTEGER DEFAULT 0
    #                                         )
    #
    # """,
    # )

    conn = None

    try:
        # connect to the database
        conn = pg.connect(**params)
        # create cursor
        cur = conn.cursor()

        # create table one by one
        print("Creating table one by one, please wait..")
        for command in commands_product:
            cur.execute(command)
        print("All tables has been created..")
        # close cursor
        cur.close()

        # commit the changes
        conn.commit()
    except (Exception, pg.DatabaseError) as e:
        print("ERROR in postgresql.py, create_tables(): ", e)
    finally:
        # close the connection
        if conn is not None:
            conn.close()

def get_pending_item():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT data, daruma_code, status, item_id, url FROM item WHERE status='not inserted'
    """


    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            d = ast.literal_eval(row[0])
            d['daruma_code'] = row[1]
            d['status'] = row[2]
            d['item_id'] = row[3]
            d['url'] = row[4]
            result.append(d)


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops get pending item: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def update_data_original(data_original, url_item):
    sql = """
       UPDATE 
       price
       SET 
       data_original=%s
       WHERE
       item_url=%s
       """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (str(data_original), url_item))

        # commit the changes
        conn.commit()
        print("data original with url %s was updated.." % url_item)

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("update_data_original error: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def update_brand_price(brand, url_item):
    sql = """
       UPDATE 
       price
       SET 
       brand=%s
       WHERE
       item_url=%s
       """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (brand, url_item))

        # commit the changes
        conn.commit()
        print("brand price with url %s was updated.." % url_item)

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("update_brand_price error: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def update_data_auto(data, url_item):

    data = ast.literal_eval(data)
    brand = matching(data['title'])
    data['brand'] = brand

    sql = """
       UPDATE 
       price
       SET 
       data_auto=%s
       WHERE
       item_url=%s
       """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (str(data), url_item))

        # commit the changes
        conn.commit()
        print("data auto with url %s was updated.." % url_item)

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("update_data_auto error: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_items():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT data, daruma_code, status FROM item WHERE spider='ProductListSpider'
    """


    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            d = ast.literal_eval(row[0])
            d['daruma_code'] = row[1]
            d['status'] = row[2]
            result.append(d)


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops get items: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_brand_from_custom(item_url):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT data_custom FROM price WHERE item_url=%s
    """


    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (item_url,))

        result = cur.fetchone()
        result = result[0] if result is not None else None
        if result:
            result = ast.literal_eval(result)
            result = result.get("brand", None)
            if result:
                if len(result) < 2:
                    result = None

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops get brand for custom: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_brand_from_data_auto(item_url):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT data_auto FROM price WHERE item_url=%s
    """


    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (item_url,))

        result = cur.fetchone()
        result = result[0] if result is not None else None
        if result:
            result = ast.literal_eval(result)
            result = result.get('brand', None)
            if result:
                if len(result) < 2:
                    result = None

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops get brand for auto: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def insert_price(name,
                 brand,
                 price_before_discount,
                 price,
                 sku,
                 supplier_code,
                 description,
                 item_url,
                 store,
                 daruma_code,
                 data,
                 tokped_product_id,
                 image_url=None,
                 store_url=None,
                 sales=None,
                 views=None,
                 reviews=None,
                 stock=0):

    if tokped_product_id:

        def get_reviews():
            try:
                return get_reviews_data(tokped_product_id)
            except:
                print("retrying to get reviews data..")
                return None

        reviews_data = get_reviews()
        while reviews_data == None:
            reviews_data = get_reviews()

        latest_date = get_latest_date_from_reviews(item_url)

        if reviews_data:
            for review in reviews_data:
                if latest_date:
                    created_date = review.get('created_at', None)
                    sub_date = latest_date - created_date
                    if sub_date.days < 0:
                        insert_review(
                            item_url=item_url,
                            reviewer=review.get('reviewer', None),
                            review=review.get('review', None),
                            rating=review.get('rating', None),
                            created_at=review.get('created_at', None)
                        )
                    else:
                        pass
                else:
                    insert_review(
                        item_url=item_url,
                        reviewer=review.get('reviewer', None),
                        review=review.get('review', None),
                        rating=review.get('rating', None),
                        created_at=review.get('created_at', None)
                    )

    if check_price(item_url=item_url) == True:
        link_id = get_link_id_by_url(item_url=item_url)
        if link_id:
            update_price_product(link_id=link_id)

        update_price_data(name=name,
                         price_before_discount=price_before_discount,
                         price=price,
                         sku=sku,
                         supplier_code=supplier_code,
                         description=description,
                         item_url=item_url,
                         store=store,
                         daruma_code=daruma_code,
                         image_url=image_url,
                         sales=sales,
                         views=views,
                         reviews=reviews,
                         stock=stock,
                         tokped_product_id=tokped_product_id)
        update_data_original(data_original=str(data), url_item=item_url)
        try:
            total_sales = str(int(sales) * int(price))
        except:
            total_sales = 0
        if _constants.PRODUCTION:
            update_elasticsearch_doc(name=name,
                                     price_before_discount=price_before_discount,
                                     price=price,
                                     sku=sku,
                                     supplier_code=supplier_code,
                                     description=description,
                                     item_url=item_url,
                                     store=store,
                                     daruma_code=daruma_code,
                                     image_url=image_url,
                                     sales=str(sales),
                                     views=str(views),
                                     reviews=str(reviews),
                                     total_sales=total_sales,
                                     brand=brand,
                                     stock=stock
                                     )
        update_item_status(url=item_url, status='inserted')
        update_store(item_url=item_url, store_name=store.split('/')[1] if len(store.split('/')) == 2 else store,
                     store_url=store_url, type=store.split('/')[1] if len(store.split('/')) == 2 else store)
        # update_row_bigquery(name=name,
        #                  brand=brand,
        #                  price_before_discount=price_before_discount,
        #                  price=price,
        #                  sku=sku,
        #                  supplier_code=supplier_code,
        #                  description=description,
        #                  item_url=item_url,
        #                  store=store,
        #                  daruma_code=daruma_code)
    else:
        try:
            total_sales = str(int(sales) * int(price))
        except:
            total_sales = 0

        if _constants.PRODUCTION:
            insert_docs(name=name,
                        brand=brand,
                        price_before_discount=price_before_discount,
                        price=price,
                        sku=sku,
                        supplier_code=supplier_code,
                        description=description,
                        item_url=item_url,
                        store=store,
                        daruma_code=daruma_code,
                        image_url=item_url,
                        store_url=store_url,
                        sales=str(sales),
                        views=str(views),
                        reviews=str(reviews),
                        total_sales=total_sales,
                        stock=stock
                        )

        sql = """
            INSERT INTO 
            price(name,
            brand,
            price_before_discount,
            price,
            sku,
            supplier_code,
            description,
            item_url,
            store,
            daruma_code,
            store_url,
            image_url,
            sales,
            views,
            reviews,
            total_sales,
            stock,
            tokped_product_id)
            VALUES(%s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s,
            %s);
            """

        conn = None
        try:
            conn = pg.connect(**params)
            total_sales = int(price) * int(sales)
            # create new cursor
            cur = conn.cursor()
            # execute sql query
            cur.execute(sql, (name,
                              brand,
                              price_before_discount,
                              price,
                              sku,
                              supplier_code,
                              description,
                              item_url,
                              store,
                              daruma_code,
                              store_url,
                              image_url,
                              sales,
                              views,
                              reviews,
                              total_sales,
                              stock,
                              tokped_product_id))

            # commit the changes
            conn.commit()
            print("%s was inserted into price table.." % (name))

            # close the cursor
            cur.close()
        except (Exception, pg.DatabaseError) as e:
            print("insert_price: error", e)
        finally:
            # close conn
            if conn is not None:
                conn.close()
                update_data_original(data_original=str(data), url_item=item_url)
                update_data_auto(data=str(data), url_item=item_url)
                brand_custom = get_brand_from_custom(item_url=item_url)
                brand_auto = get_brand_from_data_auto(item_url=item_url)
                if brand_custom:
                    update_brand_price(brand_custom, item_url)
                    update_brand_elasticsearch_doc(brand=brand_custom, item_url=item_url)
                elif brand_auto:
                    update_brand_price(brand_auto, item_url)
                    update_brand_elasticsearch_doc(brand=brand_auto, item_url=item_url)
            # while True:
            #     if isEmpty(item_url=item_url):
            #         id = get_id_and_last_update(item_url)['id']
            #         last_update = get_id_and_last_update(item_url)['last_update']
            #         print('bigquery: item not inserted, reinsert item_url %s' % item_url)
            #         push_item_to_bigquery(id, name, brand, price_before_discount, price, sku, supplier_code, description, item_url, store, last_update, daruma_code)
            #     else:
            #         print('bigquery: item %s inserted' % item_url)
            #         break


def insert_store(item_url, store_name, store_url, type):

    if check_store(item_url=item_url) == True:

        update_item_status(url=item_url, status='inserted')
        update_store(item_url=item_url, store_name=store_name.split('/')[1] if len(store_name.split('/')) == 2 else store_name,
                     store_url=store_url, type=store_name.split('/')[1] if len(store_name.split('/')) == 2 else store_name)
    else:
        sql = """
            INSERT INTO 
            store(item_url,
            name,
            store_url,
            type)
            VALUES(%s,
            %s,
            %s,
            %s);
            """

        conn = None
        try:
            conn = pg.connect(**params)

            # create new cursor
            cur = conn.cursor()
            # execute sql query
            cur.execute(sql, (item_url, store_name, store_url, type,))

            # commit the changes
            conn.commit()
            print("%s was inserted into store table.." % (store_name))

            # close the cursor
            cur.close()
        except (Exception, pg.DatabaseError) as e:
            print("error insert store: ", e)
        finally:
            # close conn
            if conn is not None:
                conn.close()

def check_price(item_url):

    sql = """
    SELECT 1 FROM price WHERE item_url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (item_url,))
        result = cur.fetchone()
        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        if result:
            return True
        else:
            return False
    except (Exception, pg.DatabaseError) as e:
        print("oops in check_price: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_link_id_by_url(item_url):

    sql = """
    SELECT link_id FROM price WHERE item_url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (item_url,))
        result = cur.fetchone()
        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        return result[0]
    except (Exception, pg.DatabaseError) as e:
        print("oops get_link_id_by_url: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def check_store(item_url):


    sql = """
    SELECT 1 FROM store WHERE item_url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (item_url,))
        result = cur.fetchone()
        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        if result:
            return True
        else:
            return False
    except (Exception, pg.DatabaseError) as e:
        print("oops check_store: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def update_store(item_url, store_name, store_url, type):
    # item_url
    # VARCHAR,
    # name
    # VARCHAR,
    # store_url
    # VARCHAR,
    # type
    # VARCHAR,


    sql = """
    UPDATE 
    store
    SET 
    name=%s,
    store_url=%s,
    type=%s
    WHERE
    item_url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (store_name, store_url, type, item_url,))

        # commit the changes
        conn.commit()
        print("%s was inserted into store table.." % (store_name))

        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("update_store error: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def update_item_status(url, status):

    sql = """
    UPDATE item SET status=%s WHERE url=%s AND status='not inserted'
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (status, url,))

        # commit the changes
        conn.commit()
        print("%s status was updated.." % (url))

        # close the cursor
        cur.close()
    # except (Exception, pg.DatabaseError) as e:
    #     print("error update item status: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def update_price_data(name,
                     price_before_discount,
                     price,
                     sku,
                     supplier_code,
                     description,
                     item_url,
                     store,
                     daruma_code,
                     image_url,
                     sales,
                     views,
                     reviews,
                     stock,
                     tokped_product_id):
    sql = """
    UPDATE price SET 
    name=%s, 
    price_before_discount=%s,
    price=%s,
    sku=%s,
    supplier_code=%s,
    description=%s,
    store=%s,
    daruma_code=%s,
    last_update=%s,
    image_url=%s,
    sales=%s,
    views=%s,
    reviews=%s,
    total_sales=%s,
    stock=%s,
    tokped_product_id=%s 
    WHERE 
    item_url=%s
    """

    conn = None
    try:
        conn = pg.connect(**params)
        total_sales = int(price) * int(sales)
        # create new cursor
        cur = conn.cursor()
        # execute sql query
        last_update = datetime.now()
        cur.execute(sql, (name,
                          price_before_discount,
                          price,
                          sku,
                          supplier_code,
                          description,
                          store,
                          daruma_code,
                          last_update,
                          image_url,
                          sales,
                          views,
                          reviews,
                          total_sales,
                          stock,
                          tokped_product_id,
                          item_url))

        # commit the changes
        conn.commit()
        print("%s store was updated.." % (item_url))

        # close the cursor
        cur.close()
    # except (Exception, pg.DatabaseError) as e:
    #     print("oops update_price_data: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def get_price():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM price ORDER BY id DESC
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount=row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            store_url = row[12] if row[12] else row[9]
            store_name = row[9]
            result.append(dict(
                price_id=price_id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_url=store_url,
                store_name=store_name
            ))




        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_test_price():
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM test_price ORDER BY id DESC
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount=row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            url_parse = urlparse(url=item_url).netloc
            store_url = get_store(item_url=item_url).get('store_url', url_parse)
            store_name = get_store(item_url=item_url).get('name', url_parse)
            result.append(dict(
                price_id=price_id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_url=store_url,
                store_name=store_name
            ))




        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_latest_date_from_reviews(item_url):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT created_at FROM reviews WHERE item_url=%s ORDER BY created_at DESC
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (item_url,))

        rows = cur.fetchall()

        for row in rows:
            result.append(
                dict(
                    created_at=row[0]
                )
            )




        # close the cursor
        cur.close()
        if not result:
            return None
        else:
            return result[0]['created_at']
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_price_by_id(price_id):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM price WHERE id=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (str(price_id),))

        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount=row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            url_parse = urlparse(url=item_url).netloc
            store_url = get_store(item_url=item_url).get('store_url', url_parse)
            store_name = get_store(item_url=item_url).get('name', url_parse)
            d = dict(
                price_id=price_id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code,
                store_url=store_url,
                store_name=store_name
            )




        # close the cursor
        cur.close()
        return d
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price_by_id: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_price_by_url(item_url):
    """ query data from the vendors table """
    conn = None
    result = list()
    query = """
    SELECT * FROM price WHERE item_url=%s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (item_url,))

        rows = cur.fetchall()

        for row in rows:
            price_id = row[0]
            name = row[1]
            brand = row[2]
            price_before_discount=row[3]
            price = row[4]
            sku = row[5]
            supplier_code = row[6]
            description = row[7]
            item_url = row[8]
            store = row[9]
            last_update = row[10].strftime('%m/%d/%Y %H:%M')
            daruma_code = row[11] if row[11] is not None else "-"
            # created_at = row[3].strftime('%m/%d/%Y %H:%M') if row[5] is not None else "-"
            result.append(dict(
                price_id=price_id,
                name=name,
                brand=brand,
                price_before_discount=price_before_discount,
                price=price,
                sku=sku,
                supplier_code=supplier_code,
                description=description,
                item_url=item_url,
                store=store,
                last_update=last_update,
                daruma_code=daruma_code
                # created_at=created_at
            ))


        # close the cursor
        cur.close()
        return result[0]
    except (Exception, pg.DatabaseError) as e:
        print("oops get_price_by_url: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_store(item_url):
    """ query data from the vendors table """
    conn = None
    result = dict()
    query = """
    SELECT * FROM store WHERE item_url=%s
    """

    # item_url
    # name
    # store_url
    # type

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (item_url,))

        rows = cur.fetchall()

        for row in rows:
            item_url = row[0]
            name = row[1]
            store_url = row[2]
            type=row[3]
            result.update(dict(
                item_url=item_url,
                name=name,
                store_url=store_url,
                type=type,
            ))


        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_store: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()

def get_id_and_last_update(item_url):
    """ query data from the vendors table """
    conn = None
    query = """
    SELECT id, last_update FROM price WHERE item_url=%s
    """

    # item_url
    # name
    # store_url
    # type

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query, (item_url,))

        result = cur.fetchone()


        # close the cursor
        cur.close()
        return dict(id=result[0], last_update=result[1])
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_store: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def get_reviews_data(product_id):
    def switch_month(month):
        m = dict(
            Jan=1,
            Feb=2,
            Mar=3,
            Apr=4,
            May=5,
            Jun=6,
            Jul=7,
            Aug=8,
            Sep=9,
            Oct=10,
            Nov=11,
            Dec=12
        )

        return m.get(month, 1)

    page_api = 1
    result = list()
    while True:
        url = 'https://www.tokopedia.com/reputationapp/review/api/v2/product/%s?page=%s&total=10' % (
        product_id, page_api)
        reviews = requests.get(url=url).json()
        for i in reviews['data']['list']:
            string_time = i['time_format']['date_time_fmt1']
            date = string_time[:2]
            month = switch_month(string_time[3:6])
            year = string_time[7:11]
            hour = string_time[13:15]
            minute = string_time[16:19]
            time = datetime(int(year), int(month), int(date), int(hour), int(minute))
            result.append(dict(
                reviewer=i['reviewer']['full_name'],
                review=i['message'],
                rating=i['rating'],
                created_at=time
            ))
        next_page = reviews['header']['links']['related']['is_has_next']
        if not next_page:
            break

        page_api += 1
    return result

def insert_review(item_url, reviewer, review, rating, created_at):

    sql = """
    INSERT INTO reviews(
    item_url,
    reviewer, 
    review, 
    rating, 
    created_at
    ) 
    VALUES(%s, 
    %s, 
    %s, 
    %s, 
    %s
    )
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (item_url, reviewer, review, rating, created_at,))

        # commit the changes
        conn.commit()
        print("%s was inserted into reviews table.." % item_url)

        # close the cursor
        cur.close()
    # except (Exception, pg.DatabaseError) as e:
    #     print("error insert review: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()

def dropTable(tableName):

    conn = None
    query = "DROP TABLE IF EXISTS {}".format(tableName)
    try:
        conn = pg.connect(**params)
        cur = conn.cursor()
        cur.execute(query)

        cur.close()
        conn.commit()
        print("%s table was deleted" % tableName)

    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def download_price_as_csv():

    conn = None
    query = """
    SELECT * FROM price ORDER BY id ASC
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        outputquery = "COPY ({0}) TO STDOUT WITH CSV HEADER".format(query)

        with open('resultfile.csv', 'w') as f:
            cur.copy_expert(outputquery, f)

        print("price table was downloaded..")
        # close the cursor
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("oops in get_price: ", e)
    finally:
        # close the conn
        if conn is not None:
            conn.close()


def run_service():
    credentials = os.environ.get('GOOGLE_APPLICATION_CREDENTIALS', None)
    if credentials:
        pass
    else:
        print('GOOGLE_APPLICATION_CREDENTIALS variable not available, creating GOOGLE_APPLICATION_CREDENTIALS variable..')
        os.system('export GOOGLE_APPLICATION_CREDENTIALS="%s/service-account-file.json"' % os.getcwd())
        print('creating GOOGLE_APPLICATION_CREDENTIALS variable done..')
        print('PATH: "%s/service-account-file.json"' % os.getcwd())
    while True:
        items = get_pending_item()
        if len(items) != 0:
            for i in items:
                if i['spider'] == 'ProductListSpider':
                    reviews = i.get('reviews', 0)
                    insert_price(name=i['title'],
                                 brand=i['brand'],
                                 price_before_discount=i['price_before_discount'],
                                 price=i['price'],
                                 sku=i['sku'],
                                 supplier_code=i['supplier_code'],
                                 description=i.get("description", None),
                                 item_url=i['url'],
                                 store=i['seller'],
                                 daruma_code=i.get("daruma_code", None),
                                 store_url=i.get('seller_url', str(i['seller']).replace(".com", "")),
                                 image_url=i.get('image_url', '-'),
                                 sales=i.get('sales', 0),
                                 views=i.get('views', 0),
                                 reviews=int(str(reviews).replace("rb", '00').replace(",", "") if "," in reviews else str(reviews).replace("rb", '000')),
                                 stock=i.get('stock', 0),
                                 data=str(i),
                                 tokped_product_id=i.get('tokped_product_id', None))
                    insert_store(item_url=i['url'], store_name=i.get("seller", None).split('/')[1] if len(
                        i.get("seller", None).split('/')) == 2 else i['seller'],
                                 store_url=i.get("seller_url", i['seller']),
                                 type=i.get("seller", None).split('/')[1] if len(
                                     i.get("seller", None).split('/')) == 2 else i['seller'])
                    update_item_status(url=i['url'], status="inserted")
                print("all items has been inserted into price table..")
        else:
            print("no items in database..")
        for i in reversed(range(1,6)):
            print("service restarting in %s.." % i)
            time.sleep(1)

def delete_item(item_id):

    conn = None
    query = """
    DELETE FROM item WHERE item_id = %s
    """

    try:
        conn = pg.connect(**params)
        cur = conn.cursor()

        cur.execute(query, (item_id,))

        conn.commit()
        print("id %s was deleted" % id)
        cur.close()
    except (Exception, pg.DatabaseError) as e:
        print("Oops: ", e)
    finally:
        if conn is not None:
            conn.close()

def get_price_from_price_table(link_id):


    sql = """
    SELECT price, views, total_sales FROM price WHERE link_id=%s
    """

    conn = None
    result = list()
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (link_id,))
        price = cur.fetchall()

        for p in price:
            price = p[0]
            views = p[1]
            total_sales = p[2]
            result.append(dict(
                price=price,
                views=views,
                total_sales=total_sales
            ))

        # close the cursor
        cur.close()
        return result
    except (Exception, pg.DatabaseError) as e:
        print("insert_price_data_product error: ", e)
    finally:
        # close conn
        if conn is not None:
            conn.close()


def insert_calculation_price_to_product(link_id, price_data):

    def get_average(lst):
        return sum(lst) / len(lst)

    price_list = [int(i.get('price')) for i in price_data]
    views = [int(i.get('views')) for i in price_data if i.get('views') is not None]
    total_sales = [int(i.get('total_sales')) for i in price_data if i.get('total_sales') is not None]

    try:
        average = get_average(price_list)
    except:
        average = 0
    min_price = min(price_list)
    max_price = max(price_list)
    total_views = sum(views)
    total_sales = sum(total_sales)

    sql = """
    UPDATE product SET price_average=%s, min_price=%s, max_price=%s, total_views=%s, total_sales=%s WHERE link_id=%s;
    """

    conn = None
    try:
        conn = pg.connect(**params)

        # create new cursor
        cur = conn.cursor()
        # execute sql query
        cur.execute(sql, (average, min_price, max_price, total_views, total_sales, link_id))
        # price_data = price_data[0] if price_data is not None else None
        # commit the changes
        conn.commit()

        # close the cursor
        cur.close()
        print("insert_calculation_price_to_product: price data for link_id %s was updated" % link_id)
    except (Exception, pg.DatabaseError) as e:
        print("insert_calculation_price_to_product error: ", e)
    finally:
        # close conn        
        if conn is not None:
            conn.close()

def update_price_product(link_id):
    price_list = get_price_from_price_table(link_id=link_id)
    insert_calculation_price_to_product(link_id=link_id, price_data=price_list)


if __name__ == "__main__":
    # connect()
    # print(json.dumps(get_item()[0], indent=4))
    # insert_items_to_price()

    try:
        run_service()
    except:
        print("rerun..")
        run_service()
    # now = datetime.now()
    # latest_date = get_latest_date_from_reviews('https://www.tokopedia.com/darumacoid/sanwa-mini-ball-valve-model-bv-15-mm-size-1-2')
    # subtract = latest_date - now
    # print(subtract.days)
    # product_id = '258905468'
    # page_api=1
    # url = 'https://www.tokopedia.com/reputationapp/review/api/v2/product/%s?page=%s&total=10' % (product_id, page_api)
    # result = get_reviews_data('245724137')
    # pprint.pprint(requests.get(url=url).json())
    # pprint.pprint(result)
    # print(get_pending_item()[0]['url'])
    # print(get_store('https://www.monotaro.id/corp_id/p102280722.html'))
    # print(get_link_id_by_url(item_url='https://www.tokopedia.com/pusatteknikcs/mesin-poles-mobil-nrt-pro-gv5000hd-mesin-poles-mobil-5') == None)
    # dropTable('item')
    # dropTable('price')
    # create_tables()
    # print(get_store('https://www.perkakasku.com/mesin-gerinda-tangan-4-gmt-g8500-pr347.html'))
    # print(get_items())
    # print(get_test_price())
    # print(get_price()[0])
    # print(isEmpty('https://www.perkakasku.com/mesin-bor-magnet-toyoda-23mm-pr846.html'))
    # print(check_price(item_url="https://www.perkakasku.com/mesin-bor-magnet-toyoda-23mm-pr846.html"))
    # print(get_id_and_last_update('https://www.perkakasku.com/mesin-bor-magnet-toyoda-23mm-pr846.html'))
    # delete_item(item_id=836)
    # update_item_status(url="https://www.tokopedia.com/chemicalindustri/fs515-flange-sealant-loctite-51", status="inserted")
    # print(json.dumps(get_pending_item(), indent=4))
    # update_item_status(url="https://www.tokopedia.com/chemicalindustri/fs515-flange-sealant-loctite-51", status="inserted")
    # print(get_pending_item())
    # print(json.dumps(get_pending_item()[0], indent=4))
    # update_item_status(url="https://www.monotaro.id/corp_id/s000002219.html", status="inserted")
    # print(len(get_pending_item()))
    # print(json.dumps(get_items(), indent=4))
    # print("items: ", len(get_items()))
    # print("pending items: ", len(get_pending_item()))
    # print(get_price())
    # print(json.dumps(get_pending_item(), indent=4))
    # print(check_price(item_url="http://www.elevenia.co.id/prd-sony-playstation-4-pro-1tb-25270867"))
    # print(get_store(item_url="https://www.tokopedia.com/moermer/buah-lo-han-kuo-lou-han-guo-obat-batuk?trkid=f=Ca0000L000P0W0S0Sh00Co0Po0Fr0Cb0_src=search_page=1_ob=23_q=lou+han+guo_po=5_catid=2292&lt=/searchproduct%20-%20p1%20-%20product"))
    # print(len(get_price()))
    # dropTable('item')
    # url = "https://www.tokopedia.com/b0w3/ps4-slim-500gb-fortnite-bundle-garansi-resmi-sony?trkid=f=Ca0000L000P0W0S0Sh00Co0Po0Fr0Cb0_src=search_page=1_ob=23_q=fortnite+ps4_po=12_catid=2101&lt=/searchproduct%20-%20p3%20-%20product"
    # store_detail = get_store(item_url=url)
    # price_detail = get_price_by_url(item_url=url)
    # print(get_pending_item()[0])
    # delete_item(150849)
    # print(get_price_by_id(price_id=3))
    # print(json.dumps(get_price(), indent=4))
    # print(get_price_by_url(item_url="http://www.elevenia.co.id/prd-konsol-sony-playstation-ps4-pro-star-wars-battlefront-ii-bun-6433947"))
