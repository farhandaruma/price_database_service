import re

def catching(title, brand, regex=None):
    if regex:
        x = re.findall(regex, title)
    else:
        x = re.findall(brand.lower(), title.lower())
    if len(x) > 0:
        return brand
    else:
        return None



def matching(title):
    with open('brand.txt', mode='r', encoding='utf-8') as result:
        brands = str(result.read()).split('\n')

    for brand in brands:
        x = catching(title=title, brand=brand)
        if x is not None:
            return x
