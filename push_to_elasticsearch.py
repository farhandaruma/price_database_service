from elasticsearch import Elasticsearch
import datetime

HOST = '172.22.0.3'
PORT =  9200

es_object = Elasticsearch([{
            'host': HOST,
            'port': PORT
        }])

def insert_docs(name,
                 brand,
                 price_before_discount,
                 price,
                 sku,
                 supplier_code,
                 description,
                 item_url,
                 store,
                 daruma_code,
                 image_url,
                 stock,
                 store_url=None,
                 sales=None,
                 views=None,
                 reviews=None,
                 total_sales=None
                 ):
    last_update = datetime.datetime.now()

    doc = dict(
        name=name,
        brand=brand,
        price_before_discount=price_before_discount,
        price=price,
        sku=sku,
        supplier_code=supplier_code,
        description=description,
        item_url=item_url,
        store=store,
        last_update=last_update,
        daruma_code=daruma_code,
        image_url=image_url,
        store_url=store_url,
        sales=sales,
        views=views,
        reviews=reviews,
        total_sales=total_sales,
        stock=stock
    )

    try:
        es_object.index(index='price', doc_type='price', body=doc)
    except Exception as e:
        print('Error in indexing document: ', str(e))
    print("was inserted to elasticsearch")


def update_link_id_elasticsearch(link_id, id):
    try:
        es_object.update(index='price', doc_type='price', id=id,
                    body={"doc": {"link_id": link_id}})
        print("link_id for price id %s was updated" % id)
    except Exception as e:
        print("ERROR update link_id elasticsearch: %s" % e)


def update_elasticsearch_doc(name,
                 price_before_discount,
                 price,
                 sku,
                 supplier_code,
                 description,
                 item_url,
                 store,
                 daruma_code,
                 image_url,
                 brand,
                 stock,
                 store_url=None,
                 sales=None,
                 views=None,
                 reviews=None,
                 total_sales=None
                 ):
    last_update = datetime.datetime.now()
    q = {
        "script": {
            "inline": "ctx._source.name=%s; ctx._source.price_before_discount=%s; ctx._source.price=%s; ctx._source.sku=%s; ctx._source.supplier_code=%s; ctx._source.description=%s; ctx._source.store=%s; ctx._source.daruma_code=%s; ctx._source.image_url=%s; ctx._source.last_update=%s; ctx._source.store_url=%s; ctx._source.sales=%s; ctx._source.views=%s; ctx._source.reviews=%s; ctx._source.total_sales=%s; ctx._source.brand=%s; ctx._source.stock=%s" %
                      (name,
                 price_before_discount,
                 price,
                 sku,
                 supplier_code,
                 description,
                 store,
                 daruma_code,
                 image_url,
                 last_update,
                 store_url,
                 sales,
                 views,
                 reviews,
                 total_sales,
                 brand,
                 stock
                 ),
            "lang": "painless"
        },
        "query": {
            "match": {
                "item_url": item_url
            }
        }
    }

    try:
        es_object.update_by_query(body=q, doc_type='price', index='price')
        print("%s elasticsearch document was updated" % item_url)
    except Exception as e:
        print("ELASTICSEARCH update error: %s" % e)

def update_brand_elasticsearch_doc(brand, item_url):
    q = {
        "script": {
            "inline": "ctx._source.brand=%s;" % brand,
            "lang": "painless"
        },
        "query": {
            "match": {
                "item_url": item_url
            }
        }
    }

    try:
        es_object.update_by_query(body=q, doc_type='price', index='price')
        print("%s brand elasticsearch document was updated" % item_url)
    except Exception as e:
        print("ELASTICSEARCH brand update error: %s" % e)
